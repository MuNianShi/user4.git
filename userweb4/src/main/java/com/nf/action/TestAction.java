package com.nf.action;

import com.nf.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Map;

public class TestAction extends ActionSupport {
    @Override
    public String execute() throws Exception {
        System.out.println("保安说，已经检查过，直接进入test.jsp");
        return "success";

    }
}
