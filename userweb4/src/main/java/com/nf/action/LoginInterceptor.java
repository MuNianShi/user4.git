package com.nf.action;


import com.nf.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

import java.util.Map;

/*
定义拦截器有2种办法：
1.实现Interceptor接口
2.集成AbstractInterceptor抽象类
 */
public class LoginInterceptor implements Interceptor {

    public void destroy() {
        System.out.println("最后销毁");
    }

    public void init() {
        System.out.println("初始化");
    }

    public String intercept(ActionInvocation actionInvocation) throws Exception {
        System.out.println("有请求到了");
        System.out.println("进行校验，判断session是否为null");
        ActionContext context = ActionContext.getContext();
        Map<String, Object> session = context.getSession();
        User user = (User) session.get("user");
        if (user!=null){
            System.out.println("我是保安，检查过，此客户已经登录过");
            return actionInvocation.invoke();
        }else {
            System.out.println("我是保安，此客户没有登录过");
            return "login";
        }


    }
}
